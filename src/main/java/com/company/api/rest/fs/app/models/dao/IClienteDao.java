package com.company.api.rest.fs.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.company.api.rest.fs.app.models.entity.Cliente;

public interface IClienteDao extends CrudRepository<Cliente, Long> {
	
}