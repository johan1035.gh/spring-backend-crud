package com.company.api.rest.fs.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ApiRestFsAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestFsAppApplication.class, args);
	}

}
