package com.company.api.rest.fs.app.models.services;

import java.util.List;

import com.company.api.rest.fs.app.models.entity.Cliente;

public interface IClienteService {
	
	public List<Cliente> findAll();
	
	public Cliente finById(Long id);
	
	public Cliente save(Cliente cliente);
	
	public void delete(Long id);
	
	
}
